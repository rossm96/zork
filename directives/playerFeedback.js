app.directive('playerFeedback', function() {
    return {
        restrict: 'E',
        scope: {
            feedback: '='
        },
        templateUrl: 'directives/playerFeedback.html'
    }
});