app.factory('rooms', [ '$http', function($http) {
    // use apiary to feed us mock data until we get the back end setup
    return $http.get('http://private-3ca7c-zork1.apiary-mock.com/rooms')
        .success(function(data) {
            return data;
        })
        .error(function(data) {
            return data;
        });
}]);