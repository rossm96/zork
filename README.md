# ZORK #

This is a reconstruction of the popular Zork game built in Angular.js

[https://www.youtube.com/watch?v=RHi2oKrLpjk&index=2&list=PLDD70139B89105635](https://www.youtube.com/watch?v=RHi2oKrLpjk&index=2&list=PLDD70139B89105635)

### How do I get set up? ###

To set up simply clone the repo into your desired directory, navigate to the directory in terminal and run 


```
#!python

python -m SimpleHTTPServer
```


to see the app