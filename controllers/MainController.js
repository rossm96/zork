app.controller('MainController', ['rooms', function(rooms) {

	// use controller as syntax for best practice
	var main = this;

	// content
	main.title = 'Zork App';
	main.welcome = 'Welcome to Zork App, a reconstruction of the popular Zork game built in Angular.js';

	// set the room object
	rooms.success(function(data){
		main.rooms = data;

		// add one to the current position so player is outside the building to begin with
		main.currentPosition = main.rooms.length;
	});

	main.newCommand = '';
	main.description = 'You are in the land of zork, which contains a building with three rooms';

	main.runCommand = function() {
		var newCommand = main.newCommand;

		if (newCommand === '') {
			return;
		}

		main.evaluateCommand(newCommand);

		// reset the input
		main.newCommand = '';
	};

	main.evaluateCommand = function(newCommand) {
		newCommand = newCommand.toLowerCase();

		switch (newCommand) {
			case 'move north':
				main.moveNorth();
				break;

			case 'move south':
				main.moveSouth();
				break;

			case 'look at room':
				main.lookAtRoom();
				break;

			default:
				main.commandNotValid();
		}
	};

	main.moveNorth = function() {
		if (main.currentPosition < 0) {
			main.description = "Sorry, you are outside the building, you can't move any further north, try moving south";

		} else {
			main.currentPosition--;

			if (main.isInBuilding()) {
				main.description = 'You have moved north into ' + main.rooms[main.currentPosition].name;

			} else {
				main.description = 'You have moved north and left the building';
			}

		}
	};

	main.moveSouth = function() {
		if (main.currentPosition >= main.rooms.length) {
			main.description = "Sorry, you are outside the building, you can't move any further south, try moving north";

		} else {
			main.currentPosition++;

			if (main.isInBuilding()) {
				main.description = 'You have moved south into ' + main.rooms[main.currentPosition].name;

			} else {
				main.description = 'You have moved south and left the building';
			}
		}
	};

	main.lookAtRoom = function() {
		if (main.rooms[main.currentPosition]) {
			main.description = 'You are currently in ' + main.rooms[main.currentPosition].name;

		} else {
			main.description = 'You are currently outside the building, the nearest room is ' + (main.currentPosition < 0 ? 'south' : 'north');
		}
	};

	main.isInBuilding = function() {
		return main.rooms[main.currentPosition];
	};

	main.commandNotValid = function() {
		main.description = 'Sorry, that command is not valid';
	};
}]);